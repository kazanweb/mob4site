var fs = require('fs');
var args = require('yargs').argv;
var path = require('path');
var clipboardy = require('clipboardy');
var config = require('./cli.config.json').config;

if (process.argv.length <= 2) {
    console.log('Usage: ' + __filename + ' NOT PARAMETERS');
	console.log('Usage: ' + __dirname);
    process.exit(-1);
}


let dirsComponent = config.components.src;

fs.readFile(config.mainScss.src, function(err, contents) {

	if(err) {
		console.log(err)
	} else {

		let eachForDir = function(dir) {

			try {
				fs.statSync(dir);
				console.log('Директория существует!!!');
			} catch(e) {
				fs.mkdirSync(dir);
				console.log('Директория создана: ' + dir);
			}

		}

		if(args.s) {

			fs.readFile(config.styles.src + args.s + '.scss', function(err, contents) {

				if(err) {

					fs.writeFile(config.styles.src + args.s + '.scss', '.' + args.s + ' {}', function(err) {

						if(err) {
							throw err;
						}

						console.log('It\'s create!');

						fs.writeFile(config.mainScss.src, '\n@import \'' + args.s + '\'\;', {flag: 'a+'}, function(err, contents) {

							clipboardy.writeSync('.' + args.s);
							console.log(args.s + '.scss file include in style.scss!');

						});

					});

				} else {

					console.log('The file exists in the directory of styles');
					process.exit(-1);

				}

			});

		} else if(args.c) {

			fs.readdir(config.components.src, function(err, files) {

				if(err) {
					console.log('Components is undefinded');
				} else {

					var _dirs = (args.c).split(path.sep);

					_dirs.forEach(function(d, index) {
						dirsComponent += d;
						if(index < _dirs.length - 1) {
							dirsComponent += path.sep;
							eachForDir(dirsComponent);
						}
					});

					fs.readFile(dirsComponent + '.scss', function(err, contents) {

						if(err) {

							fs.writeFile(dirsComponent + '.scss', '.' + _dirs[_dirs.length - 1] + ' {}', function(err) {

								if(err) {
									throw err;
								}

								console.log('It\'s create! ' + dirsComponent + '.scss');

								fs.writeFile(config.mainScss.src, '\n@import \'../components/' + (args.c).replace(path.sep, '/') + '\'\;', {flag: 'a+'}, function(err, contents) {
									console.log(_dirs[_dirs.length - 1] + '.scss file include in style.scss!');
								});

							});

						} else {

							console.log('The file exists in the directory of styles');
							process.exit(-1);

						}

					});

					fs.readFile(dirsComponent + '.html', function(err, contents) {

						if(err) {

							fs.writeFile(dirsComponent + '.html', '{% include \'' + (args.c).replace(path.sep, '/') + '.html\' %}', function(err) {

								if(err) {
									throw err;
								}

								clipboardy.writeSync('{% include \'' + (args.c).replace(path.sep, '/') + '.html\' %}');
								console.log('It\'s create! ' + dirsComponent + '.html');

							});

						} else {

							console.log('The file exists in the directory of styles');
							process.exit(-1);

						}

					});

				}
			});

		} else if(args.d) {

			var _dirs = (args.d).split(path.sep);

			_dirs.forEach(function(d, index) {
				dirsComponent += d + path.sep;
				eachForDir(dirsComponent);
			});

		}

	}

});
