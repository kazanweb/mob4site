var mob4Site = (function () {

	var tmpl = function (opts) {

		if (opts.html) {

			var _html = '';
			var _textNode = opts.html.split(/\{\{[a-zA-Z\-\_\.\#\]\[\=\'\"\>\(\)\:\s]+\}\}/g);
			var _htmlNode = opts.html.match(/\{\{[a-zA-Z\-\_\.\#\]\[\=\'\"\>\(\)\:\s]+\}\}/g);
			var _selectors;
			var callback = opts.callback || function () { }

			if(_htmlNode) {

				_htmlNode.forEach(function (arr, index) {

					_selectors = document.querySelectorAll(arr.replace('{{', '').replace('}}', ''));

					_html += _textNode[index];

					if (_selectors.length) {

						forEach(_selectors, function (index) {
							_html += this.outerHTML;
							this.parentNode.removeChild(this);
						});

					}

				});

				_html += _textNode[_textNode.length - 1];

				opts.element.insertAdjacentHTML(opts.pos, _html);

				callback.call(opts.element);

			}

		}

		if (opts.removeSelectors) {

			_selectors = document.querySelectorAll(opts.removeSelectors);

			if (_selectors.length) {

				forEach(_selectors, function (index) {
					this.parentNode.removeChild(this);
				});

			}
		}

	}

	var forEach = function (nodes, callback) {

		Array.prototype.forEach.call(nodes, function (node, index) {
			callback.call(node, index);
		});

	}

	var addEl = function (tagname, attrs, html, parent) {

		var tag,
			i;

		tag = document.createElement(tagname);

		for (i in attrs) {
			if (attrs.hasOwnProperty(i)) {
				tag.setAttribute(i, attrs[i]);
			}
		}

		tag.innerHTML = html;

		if (parent) {
			parent.appendChild(tag);
		}

		return tag;
	};

	var insertEl = function (tagname, attrs, html, nextEl) {

		var tag,
			i;

		tag = document.createElement(tagname);

		for (i in attrs) {
			if (attrs.hasOwnProperty(i)) {
				tag.setAttribute(i, attrs[i]);
			}
		}

		tag.innerHTML = html;

		if (nextEl) {
			nextEl.parentNode.insertBefore(tag, nextEl);
		}

		return tag;
	};

	var detectMobile = /Android|iPhone|iPad|iPod|BlackBerry|WPDesktop|IEMobile|Opera Mini/i.test(navigator.userAgent) || screen.width < 768; // screen.width < 1024

	var events = {
		start: detectMobile ? 'touchstart' : 'mousedown',
		move: detectMobile ? 'touchmove' : 'mousemove',
		end: detectMobile ? 'touchend' : 'mouseup'
	};

	var head = document.querySelector('head');

	return {

		init: function () {

			var obj = this;

			this.tags = {};

			if (!detectMobile) {

				head.removeChild(document.querySelector('link.mob4Site'));
				return false;

			}

			document.documentElement.classList.add('mob4Site');

			this.checkBody(function () {
				obj.removeStyles();
				obj.addMeta();
			});

			document.onreadystatechange = function () {
				if (document.readyState == "interactive") {
					obj.closePreloader();
					obj.ready();
				}
			}

		},

		checkBody: function (callback) {

			var obj = this;

			setTimeout(function () {

				var body = document.querySelector('body');

				if (body) {
					callback();
				} else {
					obj.checkBody(callback);
				}

			}, 1);

		},

		closePreloader: function () {

			var obj = this;

			setTimeout(function () {
				if (obj.tags.preloader) {
					obj.tags.preloader.classList.add('preloader_hide');
				}
			}, 1);

		},

		removeStyles: function () {

			var links = document.querySelectorAll('link');

			forEach(links, function () {
				if (!this.classList.contains('mob4Site')) {
					if (!new RegExp('https://fonts.googleapis.com/').test(this.href)) {
						this.parentNode.removeChild(this);
					}
				}
			});

		},

		addMeta: function () {

			addEl('meta', { name: 'HandheldFriendly', content: "True" }, '', head);
			addEl('meta', { name: 'format-detection', content: "telephone=yes" }, '', head);
			addEl('meta', { name: 'apple-mobile-web-app-title', content: "Setano" }, '', head);
			addEl('meta', { name: 'MobileOptimized', content: "320" }, '', head);
			addEl('meta', { name: 'viewport', content: "width=device-width, initial-scale=1, user-scalable=no" }, '', head);
			addEl('script', { src: '/mob4site/templates/main/build/js/slideout.min.js', type: 'text/javascript' }, '', head);
			this.tags.preloader = addEl('div', { class: 'preloader' }, '<div class="preloader__inner"><div class="preloader__layer"></div><div class="preloader__text"></div></div>', document.body);

		},

		ready: function () {

			this.header();
			// this.content();
			this.footer();
			this.menuSide();

		},

		header: function () {

			var panel = document.querySelector('#panel');

			if (panel) {
				panel.parentNode.removeChild(panel);
			}

		},

		content: function () {

			var info = document.querySelector('.info');
			var sideLeft = document.querySelector('.side-left');
			var qualitySidebar = document.querySelector('.quality-sidebar');
			var yandex = document.querySelector('.yandex');

			if (sideLeft) {
				document.querySelector('.content').appendChild(sideLeft);
			}
			if (info) {
				document.querySelector('.content').appendChild(info);
			}
			if (qualitySidebar && yandex) {
				qualitySidebar.parentNode.insertBefore(yandex, qualitySidebar);
			}

		},

		footer: function () {

			// empty

		},

		route: function (regexPath, callback) {

			var path = location.pathname;
			var reg = new RegExp(regexPath);

			if (reg.test(path)) {

				callback();

			}

		},

		menuSide: function () {

			tmpl({
				element: document.querySelector('body'),
				pos: 'afterBegin',
				html: '<nav id="js-menu-slide" class="slideout-bar"><div class="slideout-bar__inner">{{.header-search}}{{.l-navigation-header}}{{.l-navigation-vert}}</div></div>'
			})

			var slideout = new Slideout({
				'panel': document.querySelector('.wrapper'),
				'menu': document.getElementById('js-menu-slide'),
				'padding': 256,
				'tolerance': 70
		    });

		}
	}

})();

mob4Site.init();
