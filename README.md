Это добавляем в самый вверх страницы после тега <head>

```php
<?
	global $USER;
	if ($USER->IsAdmin()) {
?>
	<link rel="stylesheet" href="/mob4site/templates/main/build/css/style.css" class="mob4Site">
	<script src="/mob4site/templates/main/build/js/mob4Site.min.js" type="text/javascript" class="mob4Site"></script>
<?}?>
```

Есть пару встроенных функций:

```javascript

tmpl({
	element: document.querySelector('.element'),
	pos: 'afterBegin',
	html: `<div class="menu">{{.l-navigation}}</div>`,
	removeSelectors: '{{.footer}}',
	callback: function() {
		alert(this)
	}
});

element - селектор
pos: читайем документацию тут https://developer.mozilla.org/ru/docs/Web/API/Element/insertAdjacentHTML
html: обязательно через кривые кавычки


addEl('div', { class: 'menu' }, '', document.body); вставка тега с классом меню в body
insertEl('div', { class: 'menu' }, '', document.querySelector('.footer')); вставка перед элементом .footer
```
